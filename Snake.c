#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<unistd.h>
#include<pthread.h>
#include<malloc.h>

#define HEIGHT 21
#define WIDTH 59

typedef struct{
    int x;
    int y;
}Dot;

typedef struct{
    char* key;
    int* game;
    int* direction;
    int* moving;
}Argumenti;

void clrscr(){
    printf("\e[1;1H\e[2J");
    system("clear");
}

void topBottom(){
    int i;
    printf("\033[0;33m");
    for(i=0;i<WIDTH+2;i++){
        printf("#");
    }
}

void *keypress(void* k){
    Argumenti *argumenti = k;
    while(*argumenti->game >= 1){
        if(*argumenti->game == 1){ //Igra
            if(*argumenti->moving==0){
                *argumenti->key = getchar();
                if(*argumenti->key == 27 || *argumenti->key == 'q'){
                    *argumenti->game = 2;
                    system("/bin/stty cooked");
                }
                else if((*argumenti->key == 'w' || *argumenti->key == 'W') && *argumenti->direction!=2){
                    *argumenti->direction = 0;
                    *argumenti->moving = 1;
                }
                else if((*argumenti->key == 'a' || *argumenti->key == 'A') && *argumenti->direction!=3){
                    *argumenti->direction = 1;
                    *argumenti->moving = 1;
                }
                else if((*argumenti->key == 's' || *argumenti->key == 'S') && *argumenti->direction!=0){
                    *argumenti->direction = 2;
                    *argumenti->moving = 1;
                }
                else if((*argumenti->key == 'd' || *argumenti->key == 'D') && *argumenti->direction!=1){
                    *argumenti->direction = 3;
                    *argumenti->moving = 1;
                }
            }
        }
        else if(*argumenti->game == 2){ //Exit game? y/n
            printf("\n\033[0mExit game? (y/n)");
            system("/bin/stty raw");
            *argumenti->key = getchar();
            if(*argumenti->key == 'y' || *argumenti->key == 'Y'){
                *argumenti->game = 0;
                system("/bin/stty cooked");
                printf("\n");
                clrscr();
            }
            else if(*argumenti->key == 'n' || *argumenti->key == 'N')
                *argumenti->game = 1;
        }
    }
}

void setupDot(Dot* dot, Dot* snake, int snakelen){
    srand(time(NULL));
    int inside = 0, i;
    do{
        dot->x = (int)rand()%(WIDTH-2)+1;
        dot->y = (int)rand()%(HEIGHT-2)+1;
        i = 0;
        for(i=0;i < snakelen; i++){ //Provjera sudara li se točka s ijednim dijelom zmije
            if(snake[i].x == dot->x && snake[i].y == dot->y){
                inside = 1;
                break;
            }
        }
    }while(inside == 1);
}

void moveSnake(Dot* snake, int direction, int* moving,int snakelen){
    int i;

    for(i=snakelen;i>0;i--){
        if(snake[i].x==snake[i-1].x){
            if(snake[i].y<snake[i-1].y){
                snake[i].y++;
            }
            else{
                snake[i].y--;
            }
        }
        else if(snake[i].y==snake[i-1].y){
            if(snake[i].x<snake[i-1].x){
                snake[i].x++;
            }
            else{
                snake[i].x--;
            }
        }
    }

    if(direction == 0){ //Gore
        snake[0].y-=1;
    }
    else if(direction == 1){ //Lijevo
        snake[0].x-=1;
    }
    else if(direction == 2){ //Dolje
        snake[0].y+=1;
    }
    else{ //Desno
        snake[0].x+=1;
    }
    *moving = 0;
}

void renderScreen(Dot* snake, Dot dot, int points, int level){
    clrscr();
    int i,j,k,set = 0;
    system("/bin/stty cooked");
    printf("\033[0;34mPOINTS:%d",points);
    for(i = 0; i < WIDTH-20;i++)
        printf(" ");
    printf("LEVEL:%d\n",level);
    topBottom();
    printf("\n");
    for(i=0;i<HEIGHT;i++){
        printf("\033[1m\033[0;33m#");
        for(j=0;j<WIDTH;j++){
            if(dot.y == i && dot.x == j){
                printf("\033[0m*");
            }
            else{
                for(k=0;k<50;k++){
                    if(snake[k].y == i && snake[k].x == j){
                        printf("\033[0m0");
                        set = 1;
                        break;
                    }
                }
                if(set == 0){
                    printf(" ");
                }
                else{
                    set = 0;
                }
            }
        }
        printf("\033[0;33m#\n");
    }
    topBottom();
    system ("/bin/stty raw");
    fflush(stdout);
}

void initSnake(Dot* snake){
    int i;

    for(i=0;i<50;i++){
        snake[i].x = -1;
        snake[i].y = -1;
    }

    snake[0].x = 20;
    snake[0].y = 20;
    snake[1].x = 20;
    snake[1].y = 19;
}

void dotColide(Dot* snake, Dot* dot, int smjer, int* points, int* snakelen){
     if(snake[0].x==dot->x && snake[0].y==dot->y){
        *points += 20;
        
        //Novi dio zmije pri kretanju gore
        if(smjer==0){
            snake[*snakelen].x = snake[*snakelen-1].x;
            snake[*snakelen].y = snake[*snakelen-1].y+1;
        }
        //Novi dio zmije pri kretanju lijevo
        else if(smjer==1){
            snake[*snakelen].x = snake[*snakelen-1].x+1;
            snake[*snakelen].y = snake[*snakelen-1].y;
        }
        //Novi dio zmije pri kretanju dolje
        else if(smjer==2){
            snake[*snakelen].x = snake[*snakelen-1].x;
            snake[*snakelen].y = snake[*snakelen-1].y-1;
        }
        //Novi dio zmije pri kretanju desno
        else if(smjer==3){
            snake[*snakelen].x = snake[*snakelen-1].x-1;
            snake[*snakelen].y = snake[*snakelen-1].y;
        }

        //Nova točka
        setupDot(dot, snake, *snakelen);
        //Povećanje oznake duljine zmije
        *snakelen+=1;
    }
}

void gameOver(){
    printf("\033[0m"); //Bijela boja
    printf("\n _________      ___    _____     ____     __________");
    printf("\n|  _______|    /   |  |  _  |   / _  |   |   _______|");
    printf("\n| |    ___    / /| |  | | | |  / / | |   |  |____");
    printf("\n| |   |_  |  / /_| |  | | | | / /  | |   |   ____|");
    printf("\n| |_____| | / ___  |  | | | |/ /   | |   |  |_______");
    printf("\n|_________|/_/   |_|  |_| |___/    |_|   |__________|");
    printf("\n\n");
    printf("\n _________   _       __   __________   __________");
    printf("\n|  _____  | | |     / /  |   _______| |   _____  |");
    printf("\n| |     | | | |    / /   |  |____     |  |_____| |");
    printf("\n| |     | | | |   / /    |   ____|    |   ___   _|");
    printf("\n| |_____| | | |__/ /     |  |_______  |  |   \\ \\");
    printf("\n|_________| |_____/      |__________| |__|    \\_\\");
    fflush(stdout);
    sleep(3);
    printf("\nPress any key to continue...");
}

int playAgain(){
    char k='x';
    printf("\n\nPlay again? (y/n)");
    while(k!='y' && k!='n' && k!='Y' && k!='N'){
        system ("/bin/stty raw");
        k = 'x';
        k = getchar();
    }
    if(k=='y' || k=='Y')
        return 1;
    else
        return 0;
}

int main(void){
    int i = 0, smjer = 0, game = 1, moving = 0, points = 0, snakelen = 2, level = 1; //0 - Gore, 1 - lijevo, 2 - dolje, 3 - desno
    char tipka = 'x';
    Dot dot, snake[50];
    Argumenti *argumenti;
    pthread_t thread;

    //Inacijalizacija zmije
    initSnake(snake);
    
    setupDot(&dot, snake, snakelen);
    argumenti = (Argumenti*)malloc(sizeof(Argumenti));
    argumenti->game = &game;
    argumenti->key = &tipka;
    argumenti->direction = &smjer;
    argumenti->moving = &moving;

    pthread_create(&thread, NULL, keypress, (void *)argumenti);

    system ("/bin/stty raw");
    
    while(game >= 1){
        if(game==1){ //Igra
            renderScreen(snake,dot,points,level);
            moveSnake(snake, smjer, &moving, snakelen);
            //Sudar s člankom zmije
            for(i=1;i<snakelen;i++){
               if(snake[0].x == snake[i].x && snake[0].y == snake[i].y)
                goto GAMEOVER; 
            }

            //Sudar sa zidom
            if(snake[0].x < -1 || snake[0].x>=WIDTH || snake[0].y < -1 || snake[0].y>HEIGHT){
                GAMEOVER: game = 0;
                clrscr();
                system("/bin/stty cooked");
                gameOver();
                game = playAgain();
                if(game==1){
                    initSnake(snake);
                    smjer = 0;
                    snakelen = 1;
                    level = 1;
                    points = 0;
                    pthread_create(&thread, NULL, keypress, (void *)argumenti);
                    continue;
                }
                else
                    break;
            }

            //Sudar s točkom
            dotColide(snake, &dot, smjer, &points, &snakelen);
            if(snakelen>=20){
                level++;
                snakelen=2;
                initSnake(snake);
            }

            usleep(200000/level);
        }
    }
    clrscr();
    system("/bin/stty cooked");
    return 0;
}