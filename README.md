# Snake - Console game

Simple console game made in C on Ubuntu. Purpose of this game was to interest the students in C programming. 

## Getting started

To run this game, you need Linux-based OS or any Unix-based console (eg. Cygwin). You have to compile this app with gcc (gcc -pthread Snake.c -o Snake -lm) and run it through the terminal.

## Usage (Gameplay)

To controll the snake, you have to use W, A, S, and D keys (W - Up, A - Left, S - Down, D - Right). You have to collect the dot (*) on the screen to make the snake bigger. Every 20 dots level goes up and snake gets faster. To exit the game press "q" or "esc" key and press "y."

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.


## License
This project is under MIT license